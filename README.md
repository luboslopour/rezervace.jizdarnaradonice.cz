# Jízdárna Radonice - rezervační systém, HTML&amp;CSS Template.
 **Version**: 1.0.0

## Installation
Run `$ npm install` in root direcory.

## Contributors
Luboš Lopour, [lubos@lopour.net](mailto:lubos@lopour.net)
## Scripts
build:  `$ gulp default`
watch:  `$ gulp watch`
copy assets:  `$ gulp copy`
build css:  `$ gulp css`
build js:  `$ gulp js`
copy images:  `$ gulp images`
html templates:  `$ gulp html`
svg sprites:  `$ gulp sprites`
favicon:  `$ gulp favicon`
clean:  `$ gulp clean`
## Dev Dependencies
| Name | Description |
| --- | --- |
| [bootstrap](https://ghub.io/bootstrap) |  The most popular front-end framework for developing responsive, mobile first projects on the web. |
| [fs](https://ghub.io/fs) |  This package name is not currently in use, but was formerly occupied by another package. To avoid malicious use, npm is hanging on to the package name, but loosely, and we&#39;ll probably give it to you if you want it. |
| [fullcalendar](https://ghub.io/fullcalendar) |  Full-sized drag &amp; drop event calendar |
| [gulp](https://ghub.io/gulp) |  The streaming build system |
| [gulp-clean](https://ghub.io/gulp-clean) |  A gulp plugin for removing files and folders. |
| [gulp-sass](https://ghub.io/gulp-sass) |  Gulp plugin for sass |
| [gulp-clean-css](https://ghub.io/gulp-clean-css) |  Minify css with clean-css. |
| [gulp-autoprefixer](https://ghub.io/gulp-autoprefixer) |  Prefix CSS |
| [gulp-concat](https://ghub.io/gulp-concat) |  Concatenates files |
| [gulp-sourcemaps](https://ghub.io/gulp-sourcemaps) |  Source map support for Gulp.js |
| [gulp-uglify](https://ghub.io/gulp-uglify) |  Minify files with UglifyJS. |
| [gulp-watch](https://ghub.io/gulp-watch) |  Watch, that actually is an endless stream |
| [gulp-svg-symbols](https://ghub.io/gulp-svg-symbols) |  Convert SVG files to symbols with gulp |
| [gulp-image](https://ghub.io/gulp-image) |  Optimize PNG, JPG, GIF, SVG images with gulp task. |
| [gulp-nunjucks](https://ghub.io/gulp-nunjucks) |  Compile/precompile Nunjucks templates |
| [gulp-html-beautify](https://ghub.io/gulp-html-beautify) |  This is a gulp plugin to beautify HTML files. |
| [gulp-livereload](https://ghub.io/gulp-livereload) |  Gulp plugin for livereload. |
| [jquery](https://ghub.io/jquery) |  JavaScript library for DOM operations |
| [moment](https://ghub.io/moment) |  Parse, validate, manipulate, and display dates |
| [popper.js](https://ghub.io/popper.js) |  A kickass library to manage your poppers |
