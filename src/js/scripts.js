// TOOLTIP
$('[data-toggle=tooltip]').tooltip();



// POPOVER
$('[data-toggle=popover]').popover();



// TOGGLE CHECKBOX GROUP
$('[data-toggle=checkbox]').on('change',function(){
	checkbox = $('[data-target='+$(this).attr('data-group')+']');
	if($(this).is(':checked')){
		checkbox.prop('checked',true);
	} else {
		checkbox.prop('checked',false);
	}
});
$('[data-target]').on('change',function(){
	$('[data-group='+$(this).attr('data-target')+']').prop('checked',false);
});



// DATA-CLICK-ID
$('[data-toggle-id]').on('click',function(e){
	e.preventDefault();
	$($(this).attr('data-toggle-id')).click();
})



// FULLCALENDAR
$calendar = $('#calendar');
if(!$($calendar).length == 0) {
	function calendar(){
		if(typeof calendarevents === 'undefined'){
			calendarevents = [];
		}
		$calendar.fullCalendar({
			header: {
				left: 'today prev,next',
				center: 'title',
				right: ''
			},
			eventLimit: true,
			navLinks: true,
			defaultView: 'agendaWeek',
			columnFormat: [
				'dddd\nDD.MM.YYYY',
			],
			slotLabelFormat: [
			  'HH:mm'
			],
			events: calendarEvents,
			eventRender: function( event, element, view ) {
			    element.find('.fc-title').html('<strong style="color:'+event['color']+'">'+event['title']+'</strong><br>'+event['description']);
			    element.popover({
			    	title: event['title'],
			    	content: event['popover'],
			    	trigger: 'hover',
			    	placement: 'top',
			    	container: 'body'
			    });
			    $(element).css("margin-left", "-2px");


			                  $(element).css("margin-right", "-2px");
			},
			viewRender: function(view, element){
				$('[data-calendar=title]').val($('.fc-center h2').html());
				element.find('.fc-day-grid').hide();
			}
		});
	}
	calendar();
	$(window).on('resize',function(){
		$calendar.fullCalendar('destroy');
		calendar();
	});

	$('[data-calendar=prev]').on('click',function(){
		$calendar.fullCalendar('prev');
	});
	$('[data-calendar=next]').on('click',function(){
		$calendar.fullCalendar('next');
	});
	$('[data-calendar=today]').on('click',function(){
		$calendar.fullCalendar('today');
	});
}